package com.javasolution.app.devops.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping(value = "/hello")
    public String helloWorld() {
        return "Hello world, DUPA xDDD!!!";
    }
}
